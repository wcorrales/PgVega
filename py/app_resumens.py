from re import sub
from json import loads, dumps
from decimal import Decimal
from datetime import date
from pandas import DataFrame, pivot_table
from base64 import b64encode
from subprocess import Popen, PIPE
from py.utilities import unescape, set_cell, thous


############################# CONTENT #############################
'''
defs = [{"type":"table", "defs":[{"head":"col1", "foot":"sum", "class":"xxx", "value":"r[1]/100"},
                                 {"head":"col2", ...}]},
        {"type":"pivot", "defs":{"data":{}, "index":[], "values":[], "columns":[], "aggfunc":[]}},
        {"type":"graph", "defs":{"data":{}, "graph":{}}},
        {"type":"resumen", "defs":[]}]
'''
def set_table(defs, rows):
  body = []
  foot = [[], []]
  values = {}
  for row in rows:
    tmp = []
    for i in range(len(defs)):
      tmp.append(set_cell(defs[i]['value'], row) if 'value' in defs[i] else row[i])
      if 'foot' in defs[i] and tmp[i]:
        if i not in values:
          values[i] = []
        values[i].append(tmp[i])
      if 'class' in defs[i]:
        if defs[i]['class'] == 'image':
          tmp[i] = '<img src="images/%s" />' % tmp[i]
        elif not tmp[i] or tmp[i] == '':
          tmp[i] = '<span class="'+ defs[i]['class'] +'"></span>'
        else:
          tmp[i] = sub('([^,]+)', '<span class="'+ defs[i]['class'] +'">\\1</span>', str(tmp[i]))
      else:
        if isinstance(tmp[i], int):
          tmp[i] = tmp[i]
        elif isinstance(tmp[i], float) or isinstance(tmp[i], Decimal):
          tmp[i] = thous(round(tmp[i], 2))
        elif isinstance(tmp[i], str):
          tmp[i] = unescape(tmp[i]).replace('\\\\', '<br />')
        else:
          tmp[i] = str(tmp[i]) if tmp[i] else '-'
    body.append(tmp)
  if bool(values):
    fn = lambda i: thous(round({"count": lambda x: len(x),
                                "avg": lambda x: sum(x) / len(x),
                                "sum": lambda x: sum(x),
                                "min": lambda x: min(x),
                                "max": lambda x: max(x)}[defs[i]['foot']](values[i]), 2))
    foot = [[fn(i) if 'foot' in defs[i] and i in values else '' for i in range(len(defs))],
            [defs[i]['foot'] if 'foot' in defs[i] else '' for i in range(len(defs))]]
  return {"head": [d['head'] for d in defs], "body": body, "foot": foot}


def set_pivot(defs, rows):
  values = {}
  for row in rows:
    for k in defs["data"]:
      val = set_cell(defs["data"][k], row)
      if isinstance(val, float) or isinstance(val, Decimal):
        val = round(val, 2)
      if k not in values:
        values[k] = []
      values[k].append(val)
  p = pivot_table(DataFrame(values, dtype=float), fill_value=0,
                  index=defs["index"], values=defs["values"], aggfunc=defs["aggfunc"],
                  columns=defs["columns"] if 'columns' in defs else [])
  width = (len(defs["index"]) + len(defs["values"])) * 100
  return {"width": width, "table": p.to_html().replace('<table border="1" class="dataframe">',
                                                       '<table class="table table-striped">')}


def set_graph(defs, rows):
  values = []
  for row in rows:
    tmp = {}
    for k in defs["data"]:
      val = set_cell(defs["data"][k], row)
      if isinstance(val, date):
        val = str(val)
      if isinstance(val, Decimal):
        val = float(val)
      if isinstance(val, float):
        val = round(val, 2)
      tmp[k] = val
    values.append(tmp)
  if 'vega-lite' in defs['graph']['$schema']:
    defs['graph']['data'] = {"values":values}
    p1 = Popen(['/usr/lib/node_modules/vega-lite/bin/vl2vg'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    vg, err = p1.communicate(input=dumps(defs['graph']).encode())
    if err:
      raise Exception('vl graph error!')
  else:
    defs['graph']['data'].append({"name":"table", "values":values})
    vg = dumps(defs['graph']).encode()
  p2 = Popen(['/usr/lib/node_modules/vega/bin/vg2svg'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
  svg, err = p2.communicate(input=vg)
  if err:
    raise Exception('vg graph error!')
  if 'pdf' in defs:
    graph = 'data:image/png;base64,%s' % str(b64encode(svg2png(svg)))[2:-1]
  else:
    graph = 'data:image/svg+xml;base64,%s' % str(b64encode(svg))[2:-1]
  width = defs['graph']['width'] if 'width' in defs['graph'] else 200
  return {"graph": graph, "width": width}


def set_resumen(defs, rows):
  body = []
  for row in defs:
    tmp = []
    for i in range(len(row)):
      tmp.append(set_cell(row[i], rows) if row[i] != '' else '')
      if isinstance(tmp[i], int):
        tmp[i] = thous(tmp[i])
      elif isinstance(tmp[i], float) or isinstance(tmp[i], Decimal):
        tmp[i] = thous(round(tmp[i], 2))
    body.append(tmp)
  return body


def set_content(defs, rows):
  tmp = [{"type":d['type'],
           "data":{"table": set_table,
                   "pivot": set_pivot,
                   "graph": set_graph,
                   "resumen": set_resumen}[d['type']](d['defs'], rows)} for d in defs]
  return tmp


############################# QUERYS #############################
def select_querys_schema_titles(dbObj, rq):
  return {"schema": dbObj.getRows("""
                      SELECT c.relname AS table,
                             (SELECT STRING_AGG(attname, ',') FROM pg_catalog.pg_attribute
                              WHERE attrelid=c.oid AND attnum > 0 AND NOT attisdropped) AS columns
                      FROM pg_catalog.pg_class c
                      JOIN pg_catalog.pg_namespace n ON c.relnamespace=n.oid
                      WHERE c.relkind IN ('r', 'v') AND n.nspname=%s
                      ORDER BY c.relname;""", (rq['schema'], )),
          "titles": dbObj.getRows("""SELECT code, title, MAX(LENGTH(code)) OVER()
                                     FROM querys ORDER BY code;""")}


def select_querys_data(dbObj, rq):
  return dbObj.getRow("SELECT * FROM querys WHERE code=%s;", (rq['code'], ))


def select_query(dbObj, rq):
  rows = dbObj.getRows(rq['query'], rq)
  cols = dbObj.getDescription()
  defs = loads(rq['defs']) if sub('\s', '', rq['defs']) != '[]' else \
         [{"type":"table", "defs":[{"head":c} for c in cols]}]
  return set_content(defs, rows)


def select_query_saved(dbObj, rq):
  row = dbObj.getRow("SELECT query, defs, title FROM querys WHERE code=%s;", (rq['code'], ))
  rows = dbObj.getRows(row[0].replace('\\\\', '\n'), rq)
  cols = dbObj.getDescription()
  defs = loads(row[1].replace('\\\\', '\n')) if row[1] != '[]' else \
         [{"type":"table", "defs":[{"head":c} for c in cols]}]
  if 'pdf' in rq:
    for d in defs:
      if d['type'] == 'graph':
        d['defs']['pdf'] = True
  return {"code": rq['code'], "title": row[2], "content": set_content(defs, rows)}


def insert_query(dbObj, rq):
  dbObj.execute("INSERT INTO querys VALUES (DEFAULT, %s, %s, %s, %s);",
                (rq['query'], rq['defs'], rq['code'], rq['title']))
  return select_querys_schema_titles(dbObj, rq)


def update_query(dbObj, rq):
  dbObj.execute("UPDATE querys SET query=%s, defs=%s, title=%s WHERE code=%s;",
                (rq['query'], rq['defs'], rq['title'], rq['code']))
  return select_querys_schema_titles(dbObj, rq)


def delete_query(dbObj, rq):
  dbObj.execute("DELETE FROM querys WHERE code=%s;", (rq['code'], ))
  return select_querys_schema_titles(dbObj, rq)


##################################################################
def get_resumen_buttons(dbObj, rq):
  return dbObj.getRowsAssoc("""
    SELECT code, title,
           query LIKE %s AS d1,
           query LIKE %s AS d2,
           query LIKE %s AS v,
           query LIKE %s AS c
    FROM querys WHERE code = %s;""",
    ('%%%%(d1)s%%', '%%%%(d2)s%%', '%%%%(v)s%%', '%%%%(c)s%%', rq['code']))
