from psycopg2 import connect
from json import JSONEncoder, dumps
from decimal import Decimal
from datetime import date


DBNAME = 'pgvega'
DBUSER = 'delete_user'
SCHEMA = 'activity_1'


class dbObj:
  conn = None
  cur = None
  def __init__(self):
    try:
      self.conn = connect("dbname='%s' user='%s'" % (DBNAME, DBUSER))
      self.cur = self.conn.cursor()
      self.cur.execute("SET search_path TO %s;", (SCHEMA, ))
    except Exception as e:
      raise Exception("db conn error: " + str(e))
  def rollback(self):
    self.conn.rollback()
  def execute(self, sql, args=None):
    try:
      self.cur.execute(sql, args)
    except Exception as e:
      raise Exception("db exec error: " + str(e))
  def getRow(self, sql, args=None):
    self.execute(sql, args)
    return self.cur.fetchone()
  def getRows(self, sql, args=None):
    self.execute(sql, args)
    return self.cur.fetchall()
  def getRowsAssoc(self, sql, args=None):
    self.execute(sql, args)
    cols, tmp = self.cur.description, self.cur.fetchall()
    return [{cols[i][0]:row[i] for i in range(len(cols))} for row in tmp]
  def getDescription(self):
    return [cn[0] for cn in self.cur.description]
  def __del__(self):
    self.conn.commit()
    self.cur.close()
    self.conn.close()


class jsonEncoder(JSONEncoder):
  def default(self, obj):
    if isinstance(obj, Decimal): return float(obj)
    elif isinstance(obj, date): return str(obj)
    return JSONEncoder.default(self, obj)


def wrapper(func, rq, dump=False):
  try:
    rq['schema'] = SCHEMA
    obj = dbObj()
    rs = func(obj, rq)
  except Exception as e:
    try:
      obj.rollback()
    except:
      pass
    raise e
  finally:
    try:
      del obj
    except:
      pass
  return dumps(rs, cls=jsonEncoder) if dump else rs
