from re import sub
from json import loads
from datetime import datetime, timedelta
from pytz import timezone

from xhtml2pdf import pisa
from io import StringIO, BytesIO

from py.app_resumens import select_query_saved


cr_tz = timezone('America/Costa_Rica')  # datetime.now(cr_tz)


def set_pdf(header, content):
  html = '<html><head>' + \
         '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />' + \
         '<style>.table {font-size:125%;}</style>' + \
         '<style>.table {border-collapse: collapse; border-style: hidden;}</style>' + \
         '<style>.table th {border-top:1px solid #ccc; border-bottom:1px solid #ccc;}</style>' + \
         '<style>.table th, .table td {text-align:right; vertical-align:top;}</style>' + \
         '<style>.table th:first-child, .table td:first-child {text-align:left}</style>' + \
         '</head><body>' + \
         header + \
         content + \
         "Fecha (hora): %s" % datetime.now(cr_tz).strftime("%Y-%m-%d (%H:%M:%S)") + \
         '</body></html>'
  # resultFile = open('/home/wagner/doc.pdf', 'w+b')
  # pisaStatus = pisa.CreatePDF(html, dest=resultFile)
  # resultFile.close()
  pdf = BytesIO()
  status = pisa.CreatePDF(StringIO(html), dest=pdf)
  if status.err:
    raise Exception('Error en impresion pdf!')
  return pdf.getvalue()


def print_header(dbObj, rq):
  header = '<table><tr><td style="width:550px; vertical-align:middle;">' + \
           '<h1 style="font-size:300%">Pg Vega</h1>' + \
           '</td><td style="width:150px;">' + \
           '</td></tr></table>'
  return [header]


def print_resumen(dbObj, rq):
  data = select_query_saved(dbObj, rq)
  html = '<span style="font-size:200%;">' + \
         '<h2 style="display:inline;;">' + data['title'] + '</h2>' + \
         ('-' + rq['d1'] if 'd1' in rq else '') + \
         (',' + rq['d2'] if 'd2' in rq else '') + \
         '</span>'
  width = 0
  for c in data['content']:
    d = c['data']
    if c['type'] == 'table':
      html += '</tr></table>' if width > 0 else ''
      html += '<table class="table table-striped">'
      html += '<thead><tr><th>%s</th></tr></thead>' % '</th><th>'.join(d['head'])
      html += '<tbody>'
      html += ''.join(['<tr><td>%s</td></tr>' % '</td><td>'.join(r) for r in d['body']])
      html += '</tbody>'
      if len(d['foot']) > 0:
        html += '<tfoot><tr><th>%s</th></tr></tfoot>' % '</th><th>'.join(d['foot'][0])
      html += '</table>'
      width = 0
    elif c['type'] == 'pivot':
      html += '<table><tr>' if width == 0 else ''
      html += '<td>%s</td>' % d['table']
      html += '</tr></table>' if (width + d['width']) > 400 else ''
      width = 0 if (width + d['width']) > 400 else width + d['width']
    elif c['type'] == 'graph':
      html += '<table><tr>' if width == 0 else ''
      html += '<td><img src="%s" /></td>' % d['graph']
      html += '</tr></table>' if (width + d['width']) > 400 else ''
      width = 0 if (width + d['width']) > 400 else width + d['width']
  html += '</table>'
  return [html]


def print_pdf(dbObj, rq):
  rq['pdf'] = True
  return set_pdf(print_header(dbObj, rq)[0], print_resumen(dbObj, rq)[0])
