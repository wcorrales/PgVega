-- psql granjalibre -v activity='1' -f db.sql
-- \set activity '1'

CREATE SCHEMA activity_:activity;

SET search_path TO activity_:activity;


/******************************************************************/
/**************************** TABLES ******************************/
/******************************************************************/
CREATE TABLE querys (
  id SERIAL PRIMARY KEY NOT NULL,
  query TEXT NOT NULL,
  defs TEXT NOT NULL,
  code VARCHAR(50) UNIQUE NOT NULL,
  title VARCHAR(100) UNIQUE NOT NULL,
  CHECK (TRIM(query) <> '' AND query !~* '[^a-z0-9 _\]\[{}()"'';:,.&|?!~$%<>=/*+-\\^]+'),
  CHECK (TRIM(defs) <> '' AND defs !~* '[^a-z0-9 _\]\[{}()"'';:,.&|?!~$%<>=/*+-\\^]+'),
  CHECK (TRIM(code) <> '' AND code !~* '[^a-z0-9_]+'),
  CHECK (TRIM(title) <> '' AND title !~* '[^a-z0-9 @#&()"'';:,.%<>=/*+-]+')
);


GRANT USAGE ON SCHEMA activity_:activity TO select_user;
GRANT SELECT ON ALL TABLES IN SCHEMA activity_:activity TO select_user;

GRANT USAGE ON SCHEMA activity_:activity TO update_user;
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA activity_:activity TO update_user;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA activity_:activity TO update_user;

GRANT USAGE ON SCHEMA activity_:activity TO delete_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA activity_:activity TO delete_user;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA activity_:activity TO delete_user;


/*******************************************************************/
/************************** FUNCTIONS ******************************/
/*******************************************************************/




/******************************************************************/
/************************** TRIGGERS ******************************/
/******************************************************************/




/******************************************************************/
/*************************** QUERIES ******************************/
/******************************************************************/
