#!/usr/bin/env python3
from bottle import app, run, route, request, response, static_file
from py.db import wrapper
from py.app_resumens import *
from py.app_print import print_pdf


################## PATHS ###################
@route('/<directory:re:.+>/<filename:re:.+>')
def paths_handler(directory, filename):
  return static_file(filename, directory)


#################### MAIN ####################
@route('/', template='index.html')
def home_handler():
  return dict(rs={"app":"querys"})


@route('/<resumen_code>', template='index.html')
def resumen_handler(resumen_code):
  return dict(rs={"app": "resumens",
                  "resumen_buttons": wrapper(get_resumen_buttons, {"code":resumen_code}, True)})


@route('/db', method='POST')
def db_handler():
  try:
    rs = wrapper(globals()[request.forms['action']], request.forms, True)
  except Exception as e:
    response.status = 500
    rs = "db error -> " + str(e)
  return rs


@route('/pdf', method='POST')
def pdf_handler():
  try:
    rs = wrapper(print_pdf, request.forms, False)
    response.content_type = 'application/pdf'
  except Exception as e:
    response.status = 500
    rs = "db error -> " + str(e)
  return rs


###########################################
if __name__ == "__main__":
  run(host='127.0.0.1', port=9001, reloader=True)
else:
  application = app()
