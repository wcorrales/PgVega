# PgVega

Postgresql interface to vega and vega-lite written in python.  
It is very tiny and easy embeddable inside of any program.

Instalation:

    apt-get install postgresql python3-psycopg2 python3-tz python3-bottle
    apt-get install python3-numpy python3-pip python3-cffi python3-reportlab
    pip install --upgrade pip
    pip install pandas xhtml2pdf pytelegrambotapi cairosvg
    
    apt-get install build-essential libssl-dev curl
    curl -sL https://deb.nodesource.com/setup_10.x | bash -
    apt-get install nodejs
    npm install -g vega vega-lite


Postgresql configuration:

    createuser -s user
    createdb pgvega
    psql pgvega -v activity='1' -f db.sql


Example:

![PgVega - postgresql, python, vega, vega-lite](/pgvega.png?raw=true "PgVega example")
